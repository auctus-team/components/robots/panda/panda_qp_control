// Copyright (c) 2017 Franka Emika GmbH0
// Use of this source code is governed by the Apache-2.0 license, see LICENSE
#include <panda_qp_control/panda_torque_control.h>
#include <cmath>
#include <memory>
#include <chrono> 
#include <controller_interface/controller_base.h>
#include <franka/robot_state.h>
#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>

using namespace std;

namespace panda_qp_control{


bool TorqueController::init(hardware_interface::RobotHW* robot_hardware, ros::NodeHandle& node_handle) {    
    this->node_handle = node_handle;
    auto ns = this->node_handle.getNamespace();
    parent_ns = ns.substr(0, ns.find('/',1)) ;
    parent_node_handle = ros::NodeHandle(parent_ns);
    std::cout << "Parent namespace : " << parent_ns << std::endl;
    //--------------------------------------
    // LOAD ROBOT
    //--------------------------------------
    string arm_id;
    if (!node_handle.getParam("arm_id", arm_id)) {
        ROS_ERROR_STREAM("Could not read parameter arm_id");
        return false;
    }   

    std::vector<std::string> joint_names;
    if (!node_handle.getParam("joint_names", joint_names)) {
        ROS_ERROR("Could not parse joint names");
    }
    if (joint_names.size() != 7) {
        ROS_ERROR_STREAM("Wrong number of joint names, got "
        << joint_names.size() << " instead of 7 names!");
        return false;
    }
   
    auto state_interface = robot_hardware->get<franka_hw::FrankaStateInterface>();
    if (state_interface == nullptr) {
        ROS_ERROR("Could not get state interface from hardware");
        return false;
    }
    try {
        state_handle_ = std::make_unique<franka_hw::FrankaStateHandle>( state_interface->getHandle(arm_id+"_robot"));
    } catch (const hardware_interface::HardwareInterfaceException& e) {
        ROS_ERROR_STREAM(
            "Exception getting state handle: " << e.what());
            return false;
    }

    auto* effort_joint_interface = robot_hardware->get<hardware_interface::EffortJointInterface>();
    if (effort_joint_interface == nullptr)
    {
        ROS_ERROR_STREAM(
            "Error getting effort joint interface from hardware");
        return false;
    }
    for (size_t i = 0; i < 7; ++i)
    {
        try 
        {
            joint_handles_.push_back(effort_joint_interface->getHandle(joint_names[i]));
        }
        catch (const hardware_interface::HardwareInterfaceException& ex)
        {
            ROS_ERROR_STREAM(
            "Exception getting joint handles: " << ex.what());
            return false;
        }
    }

    pose_curr_publisher.init(parent_node_handle, "current_cartesian_pose", 1);
    vel_curr_publisher.init(parent_node_handle, "current_cartesian_velocity", 1);
    
    return true;
}

void TorqueController::starting(const ros::Time&)
{
      // ROS_WARN_STREAM("Starting QP Controller on the real Panda");

    std::string robot_description;
    getRosParam(parent_ns+"/robot_description", robot_description);
    std::vector<std::string> joint_names;
    ros::param::get(node_handle.getNamespace()+"/joint_names", joint_names);
    p_gains.resize(6);
    getRosParam(node_handle.getNamespace()+"/p_gains", p_gains);
    d_gains.resize(6);
    getRosParam(node_handle.getNamespace()+"/d_gains", d_gains);
    dtorque_max.resize(7);
    getRosParam(node_handle.getNamespace()+"/dtorque_max", dtorque_max);
    kp_reg.resize(7);
    getRosParam(node_handle.getNamespace()+"/kp_reg", kp_reg);

    franka::RobotState robot_state = state_handle_->getRobotState(); 
    //Get robot current state
    Eigen::Matrix<double,7,1>  q_init(robot_state.q.data());

    if (!initialized) // franka_control starts twice but the first one doesn't initialize the torque_qp ...
    {
        initialized = torque_qp.init(robot_description,joint_names) ;
        torque_qp.setRegularizationGains(kp_reg);
        torque_qp.setDTorqueMax(dtorque_max);

        trajectory.interface->init(parent_node_handle,"robot_description",q_init,joint_names);
        model = torque_qp.getRobotModel();
        data = pinocchio::Data(model);
    }
}


void TorqueController::update(const ros::Time&, const ros::Duration& period) {    

    //--------------------------------------
    // ROBOT STATE
    //--------------------------------------
    // get state variables
    franka::RobotState robot_state = state_handle_->getRobotState(); 
    //Get robot current state
    Eigen::Matrix<double,7,1>  q(robot_state.q.data());
    Eigen::Matrix<double,7,1>  qd(robot_state.dq.data());
    Eigen::Matrix<double,7,1>  tau_J_d(robot_state.tau_J_d.data());

    // Update model
      // First calls the forwardKinematics on the model, then computes the placement of each frame.
    pinocchio::forwardKinematics(model,data,q,qd);
    pinocchio::updateFramePlacements(model,data);
    pinocchio::Motion xd = pinocchio::getFrameVelocity(model, data, model.getFrameId(torque_qp.getControlledFrame()),pinocchio::ReferenceFrame::LOCAL);
    
    //Update the trajectory
    trajectory.interface->updateTrajectory();
    pinocchio::SE3 oMdes(trajectory.interface->getCartesianPose().matrix());
    
    // Compute error
    const pinocchio::SE3 tipMdes = data.oMf[model.getFrameId(torque_qp.getControlledFrame())].actInv(oMdes);
    Eigen::Matrix<double,6,1> err =  pinocchio::log6(tipMdes).toVector();
    
    // Proportional controller with feedforward
    // Eigen::Matrix<double,6,1> xdd_star = p_gains.cwiseProduct(err) + (2.0 *p_gains.cwiseSqrt()).cwiseProduct( trajectory.interface->getCartesianVelocity() - xd.toVector() ) + trajectory.interface->getCartesianAcceleration();
    Eigen::Matrix<double,6,1> xdd_star = p_gains.cwiseProduct(err) + d_gains.cwiseProduct(  - xd.toVector() ) + trajectory.interface->getCartesianAcceleration();

    Eigen::Matrix<double,7,1>  joint_command = torque_qp.update(q,qd,tau_J_d,xdd_star);

    for (size_t i = 0; i < 7; ++i)
    {
        joint_handles_[i].setCommand(joint_command(i));
    }      
    publishCartesianState();
    
}

void TorqueController::publishCartesianState()
{
    Eigen::Affine3d cartesian_pose;
    cartesian_pose.matrix() = data.oMf[model.getFrameId(torque_qp.getControlledFrame())].toHomogeneousMatrix();
    tf::poseEigenToMsg(cartesian_pose,cartesian_pose_msg);
    xd = pinocchio::getFrameVelocity(model, data, model.getFrameId(torque_qp.getControlledFrame()),pinocchio::ReferenceFrame::LOCAL_WORLD_ALIGNED );
    tf::twistEigenToMsg(xd.toVector(), cartesian_velocity_msg);
    //Publish robot desired pose
    if (pose_curr_publisher.trylock())
    {
        pose_curr_publisher.msg_.header.stamp = ros::Time::now();
        pose_curr_publisher.msg_.header.frame_id = "world";
        pose_curr_publisher.msg_.pose = cartesian_pose_msg;
        pose_curr_publisher.unlockAndPublish();
    }
    //Publish robot desired velocity
    if (vel_curr_publisher.trylock())
    {
        vel_curr_publisher.msg_.header.stamp = ros::Time::now();
        vel_curr_publisher.msg_.header.frame_id = "world";
        vel_curr_publisher.msg_.twist = cartesian_velocity_msg;
        vel_curr_publisher.unlockAndPublish();
    }
}
}  // namespace panda_qp_control

PLUGINLIB_EXPORT_CLASS(panda_qp_control::TorqueController,
controller_interface::ControllerBase)
